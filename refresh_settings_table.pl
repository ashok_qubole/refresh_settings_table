#!/usr/bin/perl

use strict;
use warnings;

# Description: recieves env details and run mode via karma job,
#              fetches settings table using qds-ops and comapres both envs,
#              generates SQL file with insert, update and delete statements,
#              transfers and executes SQL file against targetted env
#
# Input params: could be set via karma params or env variables
#
#   $copy_from_env             env to copy settings from, env name used in qds-ops command, e.g. qa3, api2
#   $copy_to_env                 env to copy settings to, env name used in qds-ops command, e.g. qa, qib-uid
#   $copy_from_jumphost        jumphost for env to copy settings from, e.g. nandi2.prod.qubole.net
#   $copy_to_jumphost         jumphost for env to copy settings to, e.g. admin.staging.qubole.net
#   $dry_run                    supported values are 1 or 0, if 1 sql script is generated but not executed
#   $run_mode                supported values are
#                              update     update existing keys but don't insert or delete any key
#                              insert     update existing keys and insert new ones but don't delete any key
#                              delete     update existing keys, insert new ones and delete extra keys

my $copy_to_env        = $ENV{'$copy_to_env'};
my $copy_from_env      = $ENV{'$copy_from_env'};
my $copy_to_jumphost   = $ENV{'$copy_to_jumphost'};
my $copy_from_jumphost = $ENV{'$copy_from_jumphost'};
my $dry_run            = $ENV{'$dry_run'};
my $run_mode           = $ENV{'$run_mode'};

# Config params:
#   $protected_keys            list of settings which are not be fetched from DB, due to security or other reasons
#   $ignored_keys            list of settings which are expected to change, no sql will be generated for them e.g. env related
#   $ignored_patterns        same as $ignored_keys but a regex is used to filter multiple settings out, e.g. tunnel.*tunneling_gateway

my $protected_keys = [
    'aws.ec2_access_key',
    'aws.ec2_secret_key',
    'aws.ec2_ro_access_key',
    'aws.ec2_ro_secret_key',
    'aws.root_access_key',
    'aws.root_secret_key',
    'aws.access_key_for_assume_role_of_customer',
    'aws.secret_key_for_assume_role_of_customer',
    'ga_outh.client_id',
    'ga_outh.client_secret',
    'google_api.app_id',
    'google_api.secret',
    'mail.password',
    'metastore.dbadmin_passwd',
    'oozie.db_passwd',
    'smtp.password',
    'stripe.public_key',
    'stripe.secret_key',
    'tapp.aws_saas_marketplace_role_arn',
    'tapp.google_custom_search_api_key',
    'tapp.google_search_cx_key',
    'tapp.jira_password',
    'zendesk.oauth_token',
    'zendesk.password'
];

my $ignored_keys = [
    'alert.env_qubole_tier_mail_id',
    'aws.as_group_name',
    'aws.elb_name',
    'aws.pemfile',
    'aws.route53_zoneid',
    'aws.userid',
    'aws_account_id',
    'ga.enable_analytics',
    'ga.web_property_id',
    'gce.compute_sa_client_email',
    'gce.compute_sa_private_key',
    'gce.region',
    'hadoop.cluster_name_prefix',
    'hadoop.keyname',
    'hadoop.master_type',
    'hadoop.publickey',
    'hadoop.slave_type',
    'hive.qbol_standalone_11',
    'hive.qbol_standalone_13',
    'hive.qbol_syntax',
    'hubspot.enable',
    'hubspot.sign_up_url',
    'mojave.cloudfront_url',
    'nezha.url',
    'oozie.aws_az',
    'oozie.cluster_name',
    'oozie.dns',
    'oozie.hdfs',
    'oozie.jobtracker',
    'oozie.master_type',
    'oozie.prev_clusters',
    'oozie.slave_type',
    'pixie.dns',
    'scheduler.url',
    'shared.account.auth_token',
    'spotman.api_token',
    'shared.account.id',
    'tapp.aws_saas_entitlements_product_code',
    'tapp.cluster_management_mail_id',
    'tapp.can_create_new_accounts',
    'tapp.customer_facing_env',
    'tapp.env_owner_mail_id',
    'tapp.inspectlet_id',    
    'tapp.jira_username',
    'tapp.log_level',
    'tapp.notification_customer_alert',
    'tapp.notification_customer_success',
    'tapp.notification_sales',
    'tapp.notification_solutions',
    'tapp.public_forum_mail_id',
    'tapp.qbol_system_space_uri',
    'tapp.s3_bucket',
    'tapp.sample_db_host',
    'tenali.defloc',
    'tunnel.whitelist_machines',
    'zendesk.username'
];

my $ignored_patterns =
  [ 'tunnel.*tunnel.*', 'tapp.*surveymonkey.*', 'hubspot.user.*' ];

# Script execution starts here

# Validate inputs
unless ( defined $copy_to_env
    && defined $copy_from_env
    && defined $copy_to_jumphost
    && defined $copy_from_jumphost
    && defined $dry_run
    && defined $run_mode )
{
    die "Invalid input params: $!";
}

my $output_sql_file_name = 'diff.sql';
my $out_file;

# Loading settings table as hash, key as var column, value as value column for both envs
my $from_settings =
  load_settings_for_env( $copy_from_jumphost, $copy_from_env );
my $to_settings = load_settings_for_env( $copy_to_jumphost, $copy_to_env );

# Comparing settings hash for both envs
my ( $deleted, $same, $added ) = compare( $to_settings, $from_settings );

initialize_sql_file();
if ( $run_mode ne 'update' ) {

    # Generate insert statements
    foreach my $key (@$added) {
        if ( !ignored($key)) {
            print_diff($key, $from_settings->{$key}, 'not_set', 'INSERT');
            print_sql(
"INSERT INTO settings (var,value,created_at,updated_at) values ('$key','$from_settings->{$key}',now(),now())"
            );
        }
    }
}

if ( $run_mode eq 'delete' ) {

    # Generate delete statements
    foreach my $key (@$deleted) {
        if ( !ignored($key) ) {
            print_diff($key, 'not_set' , $to_settings->{$key} , 'DELETE');
            print_sql("DELETE FROM settings WHERE var = '$key'");
        }
    }
}

# Generate update statements
foreach my $key (@$same) {
    if (   $from_settings->{$key} ne $to_settings->{$key}
        && !ignored($key))
    {
        print_diff($key, $from_settings->{$key}, $to_settings->{$key}, 'UPDATE');
        print_sql(
"UPDATE settings set value = '$from_settings->{$key}' WHERE var = '$key'"
        );
    }
}

foreach my $key (@$ignored_keys) {
    if (exists $to_settings->{$key} && !defined $to_settings->{$key}) { $to_settings->{$key} = 'not_set'};
    if (exists $from_settings->{$key} && !defined $from_settings->{$key}) { $from_settings->{$key} = 'not_set'};
    print_diff($key, $from_settings->{$key}, $to_settings->{$key}, 'IGNORE');
}

if ( $dry_run eq "0" ) {

    # Transfer generated sql file
    execute_command("scp $output_sql_file_name $copy_to_jumphost:~");

# Execute sql file
    my $output = execute_command("ssh $copy_to_jumphost -C 'qds-ops -e ". $copy_to_env . " rds run -f $output_sql_file_name'" );
     logger("SQL execution output:\n$output");
}

# Script execution ends here
exit 0;

sub logger { print "@_\n" }

sub print_sql { print $out_file "@_;\n" }

sub print_diff { return printf("| %-50s | %-60s | %-60s | %-6s |\n",shorten($_[0],48), shorten($_[1],58),shorten($_[2], 58), $_[3]); }

sub shorten { return length($_[0]) < $_[1] ? $_[0] : substr($_[0], 0, $_[1]) . '..'; }

sub ignored {
    my ($key) = @_;
    my $found = 0;
    if ( grep ( /^$_[0]$/, @$ignored_keys ) ) { return 1; }
    foreach my $pattern (@$ignored_patterns) {
        if ( $key =~ $pattern ) { return 1; }
    }
    return $found;
}

sub compare {
    my ( $a, $b ) = @_;
    [ grep !exists $b->{$_},   sort keys %$a ],
      [ grep exists $b->{$_},  sort keys %$a ],
      [ grep !exists $a->{$_}, sort keys %$b ];
}

sub execute_command {
    logger("\nExecuting command: @_");
    return qx/@_/ or die "\nExecution failed, Command: @_\nError: $?\n";
}

sub execute_ssh_command {
    return execute_command( 'ssh ' . $_[0] . ' -C "' . $_[1] . '"' );
}

sub execute_qds_ops_get_settings {
    my $sql_command = "SELECT var, value FROM settings WHERE var NOT IN ('"
      . join( "','", @$protected_keys ) . "');";
    return execute_ssh_command( $_[0],
        'qds-ops -e ' . $_[1] . " rds run -e \\\"" . $sql_command . "\\\"" );
}

sub load_settings_for_env {
    my $settings_hash = {};
    my @response      = execute_qds_ops_get_settings(@_);
    foreach my $line (@response) {
        chomp $line;
        if ( $line =~ m/(.+?)\s+(.*)/ ) {
            unless (defined $2) { $2 = ''; }
            if ( defined $1 && $1 ne '' ) {
                $settings_hash->{$1} = $2;
            }
            else {
                warn "Regex values issue: " . $line;
            }
        }
    }
    return $settings_hash;
}

sub initialize_sql_file {
    open $out_file, ">$output_sql_file_name";
    my $backup_table = 'settings_'.time;
    logger("Settings table on $copy_to_env env will be backed up as $backup_table");
#    logger("\nBelow settings are portected and will not be feteched from DB: @$protected_keys\n");
    print_sql( "CREATE TABLE $backup_table  AS SELECT * FROM settings" );
    print_diff(    "Var", "Value in $copy_from_env", "Value in $copy_to_env", 'Action');
}

